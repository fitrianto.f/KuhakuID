import threading
from telegram.ext import CommandHandler

from bot import dispatcher
from bot.helper.mirror_utils.upload_utils.gdriveTools import GoogleDriveHelper
from bot.helper.telegram_helper.message_utils import deleteMessage, sendMessage, auto_delete_message
from bot.helper.telegram_helper.filters import CustomFilters
from bot.helper.telegram_helper.bot_commands import BotCommands
from bot.helper.ext_utils.bot_utils import is_gdrive_link, is_gdtot_link, new_thread
from bot.helper.mirror_utils.download_utils.direct_link_generator import gdtot
from bot.helper.ext_utils.exceptions import DirectDownloadLinkException

@new_thread
def countNode(update, context):
    args = update.message.text.split(" ", maxsplit=1)
    reply_to = update.message.reply_to_message
    if len(args) > 1:
        link = args[1]
        if update.message.from_user.username:
            tag = f"@{update.message.from_user.username} (<code>{update.message.from_user.id}</code>)\n#id{update.message.from_user.id}"
        else:
            tag = f'<a href="tg://user?id={update.message.from_user.id}">{update.message.from_user.first_name}</a> (<code>{update.message.from_user.id}</code>)\n#id{update.message.from_user.id}'
    elif reply_to is not None:
        link = reply_to.text
        if reply_to.from_user.username:
            tag = f"@{reply_to.from_user.username} (<code>{reply_to.from_user.id}</code>)\n#id{reply_to.from_user.id}"
        else:
            tag = f'<a href="tg://user?id={reply_to.from_user.id}">{reply_to.from_user.first_name}</a> (<code>{reply_to.from_user.id}</code>)\n#id{reply_to.from_user.id}'
    else:
        link = ''
    gdtot_link = is_gdtot_link(link)
    if gdtot_link:
        pesan = sendMessage(f"<i>⌛️ Generating GDrive link from GDTot Link..</i>", context.bot, update)
        try:
            deleteMessage(context.bot, pesan)
            link = gdtot(link)
        except DirectDownloadLinkException as e:
            deleteMessage(context.bot, pesan)
            return sendMessage(str(e), context.bot, update)
    if is_gdrive_link(link):
        msg = sendMessage(f"Menghitung: <code>{link}</code>", context.bot, update)
        gd = GoogleDriveHelper()
        result = gd.count(link)
        deleteMessage(context.bot, msg)
        cc = f'\n\n<b>🙍🏻‍♂️ Penghitung: </b>{tag}'
        sendMessage(result + cc, context.bot, update)
        if gdtot_link:
            gd.deletefile(link)
    else:
        msg = sendMessage('Silahkan ulangi lagi dengan mencantumkan link GDrive:\n<code>/hitung (spasi) link GDrive</code>\n\nJika link private, silahkan invite: kuyshare@googlegroups.com agar file bisa dihitung.', context.bot, update)
        threading.Thread(target=auto_delete_message, args=(context.bot, update.message, msg)).start()

count_handler = CommandHandler(BotCommands.CountCommand, countNode, run_async=True)
dispatcher.add_handler(count_handler)
