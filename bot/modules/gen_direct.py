import requests
import lk21
import json
from bot import dispatcher
from telegram.ext import CommandHandler
from bot.helper.telegram_helper.message_utils import sendMessage, deleteMessage, sendMarkup
from bot.helper.telegram_helper.filters import CustomFilters
from bot.helper.telegram_helper.bot_commands import BotCommands
from bot.helper.mirror_utils.download_utils.direct_link_generator import direct_link_generator
from bot.helper.ext_utils import bot_utils
from bot.helper.ext_utils.exceptions import DirectDownloadLinkException


def bypass(url):
  payload = {
    "url": url,
  }

  r = requests.post("https://api.bypass.vip/", data=payload)
  return r.json()

def short(update, context):
    args = update.message.text.split(" ", maxsplit=1)
    reply_to = update.message.reply_to_message
    x = lk21.Bypass()
    if len(args) > 1:
        link = args[1]
    elif reply_to is not None:
        link = reply_to.text
    else:
        link = None

    if update.message.from_user.username:
        uname = f'@{update.message.from_user.username}'
    else:
        uname = f'<a href="tg://user?id={update.message.from_user.id}">{update.message.from_user.first_name}</a>'
    if uname is not None:
        cc = f'\n\n🙍🏻‍♂️ <b>Shorten by:</b> {uname} - (<code>{update.message.from_user.id}</code>)\n#id{update.message.from_user.id}'

    if link is not None and bot_utils.is_url(link):
        a = sendMessage (f"<i>Generating Shortlink from </i> <code>{link}</code>..", context.bot, update)
        try:
            res = requests.get(f"https://tinyurl.com/api-create.php?url={link}").text
            deleteMessage(context.bot, a)
            return sendMessage(f"<b>Original Link:</b> {link}\n<b>ShortLink:</b>\n{res}{cc}", context.bot, update)
        except exception as e:
            deleteMessage(context.bot, a)
            return sendMessage(f"ERROR: {e}")
    else:
        return sendMessage("Give url to shorten", context.bot, update)

def direct(update, context):
    args = update.message.text.split(" ", maxsplit=1)
    reply_to = update.message.reply_to_message
    x = lk21.Bypass()
    if len(args) > 1:
        link = args[1]
    elif reply_to is not None:
        link = reply_to.text
    else:
        link = ''

    if update.message.from_user.username:
        uname = f'@{update.message.from_user.username}'
    else:
        uname = f'<a href="tg://user?id={update.message.from_user.id}">{update.message.from_user.first_name}</a>'
    if uname is not None:
        cc = f'\n\n🙍🏻‍♂️ <b>Bypasser:</b> {uname} - (<code>{update.message.from_user.id}</code>)\n#id{update.message.from_user.id}'

    if "layarkacaxxi.icu" in link or "fembed.net" in link or "fembed.com" in link or "femax20.com" in link:
       a = sendMessage (f"<i>Generating direct link from </i> <code>{link}</code>..", context.bot, update)
       try:
          e = x.bypass_fembed(link)
          res = "".join(f"{item}. {e[index]} ({index})\n" for item, index in enumerate(list(e), start=1))
          deleteMessage(context.bot, a)
          return sendMessage(f"<b>Original Link:</b> {link}\n<b>Bypassed Link:</b>\n{res}{cc}", context.bot, update)
       except Exception:
          deleteMessage(context.bot, a)
          return sendMessage("Terjadi error saat generate direct link", context.bot, update)
    if "ouo.io" in link or "ouo.press" in link or "bit.ly" in link:
       a = sendMessage (f"<i>Generating direct link from </i> <code>{link}</code>..", context.bot, update)
       try:
          bypasser = lk21.Bypass()
          res = bypasser.bypass_url(link)
          deleteMessage(context.bot, a)
          return sendMessage(f"<b>Original Link:</b> {link}\n<b>Bypassed Link:</b>\n{res}{cc}", context.bot, update)
       except Exception:
          deleteMessage(context.bot, a)
          return sendMessage("Terjadi error saat generate direct link", context.bot, update)
    if "adf.ly" in link:
       a = sendMessage (f"<i>Generating link from </i> <code>{link}</code>..", context.bot, update)
       try:
          byp = bypass(link)
          res = byp['destination']
          deleteMessage(context.bot, a)
          return sendMessage(f"<b>Original Link:</b> {link}\n<b>Bypassed Link:</b>\n{res}{cc}", context.bot, update)
       except Exception as e:
          deleteMessage(context.bot, a)
          return sendMessage(f"ERROR: {e}", context.bot, update)
    if "anonfiles.com" in link or "antfiles.com" in link or "zippyshare.com" in link or "mediafire.com" in link or "hxfile.co" in link or "letsupload.io" in link or "pixeldrain.com" in link or "krakenfiles.com" in link or "solidfiles.com" in link or "1fichier.com" in link or "racaty.net" in link or "bayfiles.com" in link or "sfile.mobi" in link or "androiddatahost.com" in link or "sourceforge.net" in link or "dropbox.com" in link:
       a = sendMessage (f"<i>Generating direct link from</i> <code>{link}</code>..", context.bot, update)
       try:
          res = direct_link_generator(link)
          deleteMessage(context.bot, a)
          return sendMessage(f"<b>Original Link:</b> {link}\n<b>Bypassed Link:</b> {res}{cc}", context.bot, update)
       except DirectDownloadLinkException as e:
          deleteMessage(context.bot, a)
          return sendMessage(f"ERROR: {e}", context.bot, update)
    if link.startswith("https://uptobox.com"):
       a = sendMessage (f"<i>Generating direct link from</i> <code>{link}</code>. Please wait for += 30s..", context.bot, update)
       try:
          res = direct_link_generator(link)
          deleteMessage(context.bot, a)
          return sendMessage(f"<b>Original Link:</b> {link}\n<b>Bypassed Link:</b> {res}{cc}", context.bot, update)
       except DirectDownloadLinkException as e:
          deleteMessage(context.bot, a)
          return sendMessage(f"ERROR: {e}", context.bot, update)
    else:
       msg = sendMessage("This command for generate direct link or bypass from url shortener, some direct link only work for this bot.\n\n<b>Supported Link:</b>\n<code>Fembed, Anonfiles, Antfiles, Zippyshare, Mediafire, Uptobox, HxFile, Letsupload, Pixeldrain, krakenfiles, Solidfiles, 1fichier, Racaty, Bayfiles, sfile.mobi, androiddatahost, Sourceforge, Dropbox, ouo.io, ouo.press, bit.ly, adf.ly</code>", context.bot, update)

direct_handler = CommandHandler(BotCommands.DirectCommand, direct, filters=CustomFilters.authorized_chat | CustomFilters.authorized_user, run_async=True)
short_handler = CommandHandler(BotCommands.ShortCommand, short, filters=CustomFilters.authorized_chat | CustomFilters.authorized_user, run_async=True)
dispatcher.add_handler(direct_handler)
dispatcher.add_handler(short_handler)
