import random
import string
from telegram import InlineKeyboardMarkup
from telegram.ext import MessageHandler, CallbackQueryHandler, Filters

from bot.helper.mirror_utils.upload_utils import gdriveTools
from bot import dispatcher, LOGGER, CLONE_LIMIT, STOP_DUPLICATE, download_dict, download_dict_lock, Interval, app, OWNER_ID
from bot.helper.ext_utils.bot_utils import get_readable_file_size, is_gdrive_link, is_gdtot_link
from bot.helper.mirror_utils.download_utils.direct_link_generator import gdtot
from bot.helper.ext_utils.exceptions import DirectDownloadLinkException
from bot.helper.telegram_helper.message_utils import *
from bot.helper.mirror_utils.status_utils.clone_status import CloneStatus
from bot.helper.telegram_helper.filters import CustomFilters
from bot.helper.telegram_helper import button_build

def auto_button(update, context):
    user_id = update.message.from_user.id
    pesan = update.message.text
    buttons = button_build.ButtonMaker()
    buttons.sbutton("Clone", f"clone {user_id} {update.message.message_id}")
    buttons.sbutton("Count", f"count {user_id} {update.message.message_id}")
    buttons.sbutton("Delete", f"delete {user_id} {update.message.message_id}")
    buttons.sbutton("Cancel", f"cancelbtn {user_id}")
    button = InlineKeyboardMarkup(buttons.build_menu(2))
    sendMarkup('Choose option.', context.bot, update, button)

def select_auto(update, context):
    query = update.callback_query
    user_id = query.from_user.id
    msg = query.message
    data = query.data
    data = data.split(" ")
    if user_id != int(data[1]):
        query.answer(text="Tombol ini bukan untukmu kawan!", show_alert=True)
    elif data[0] == 'clone':
        query.answer()
        salin_drive(query, msg, query.message.chat.id, data[2])
    elif data[0] == 'count':
        query.answer()
        count_drive(query, msg, query.message.chat.id, data[2])
    elif data[0] == 'delete':
        if user_id != OWNER_ID:
           query.answer(text="Command ini cuma buat owner yaakk", show_alert=True)
           return
        query.answer()
        delete_drive(msg, query.message.chat.id, data[2])
    elif data[0] == 'cancelbtn':
        editMessage("Proses dibatalkan!", msg)

def delete_drive(bmsg, chat_id, msg_id):
    pesan = app.get_messages(chat_id, int(msg_id))
    link = pesan.text
    if is_gdrive_link(link):
        LOGGER.info(link)
        drive = gdriveTools.GoogleDriveHelper()
        msg = drive.deletefile(link)
        editMessage(msg, bmsg)

def count_drive(query, bmsg, chat_id, msg_id):
    pesan = app.get_messages(chat_id, int(msg_id))
    link = pesan.text
    gdtot_link = is_gdtot_link(link)
    if gdtot_link:
        try:
            link = gdtot(link)
        except DirectDownloadLinkException as e:
            return editMessage(str(e), bmsg)
    if is_gdrive_link(link):
        editMessage(f"Menghitung: <code>{link}</code>", bmsg)
        gd = gdriveTools.GoogleDriveHelper()
        result = gd.count(link)
        if query.from_user.username:
            uname = f'@{query.from_user.username}'
        else:
            uname = f'<a href="tg://user?id={query.from_user.id}">{query.from_user.first_name}</a>'
        if uname is not None:
            cc = f'\n\n<b>🙍🏻‍♂️ Penghitung: </b>{uname} (<code>{query.from_user.id}</code>)\n#id{query.from_user.id}'
        editMessage(result + cc, bmsg)
        if gdtot_link:
            gd.deletefile(link)

def salin_drive(query, bmsg, chat_id, msg_id):
    pesan = app.get_messages(chat_id, int(msg_id))
    link = pesan.text
    gdtot_link = is_gdtot_link(link)
    if gdtot_link:
        try:
            editMessage(f"<i>⌛️ Generating GDrive link from GDTot Link..</i>", bmsg)
            link = gdtot(link)
        except DirectDownloadLinkException as e:
            return editMessage(str(e), bmsg)
    if is_gdrive_link(link):
        gd = gdriveTools.GoogleDriveHelper()
        res, size, name, files = gd.helper(link)
        if res != "":
            editMessage(res, bmsg)
            return
        if STOP_DUPLICATE:
            LOGGER.info('Checking File/Folder if already in Drive...')
            editMessage(f"<i>Sedang mengecek link kamu...</i>", bmsg)
            smsg, button = gd.drive_list(name, True, True)
            if smsg:
                msg3 = f"⚠ <code>{name}</code> (<code>{get_readable_file_size(size)}</code>) <u>sudah ada di GDrive.</u>"
                editMessage(msg3, bmsg, button)
                return
        if CLONE_LIMIT is not None:
            LOGGER.info('Checking File/Folder Size...')
            if size > CLONE_LIMIT * 1024**3:
                msg2 = f'Mohon maaf, limit clone file/folder {CLONE_LIMIT}GB.\n<code>{name}</code> berukuran <code>{get_readable_file_size(size)}</code>.'
                editMessage(msg2, bmsg)
                return
        if files <= 2:
            msg = editMessage(f"Sedang menyalin: <code>{link}</code> (<code>{get_readable_file_size(size)}</code>)", bmsg)
            result, button = gd.clone(link)
        else:
            editMessage(f"<i>Please wait, don't spam bot...</i>", bmsg)
            drive = gdriveTools.GoogleDriveHelper(name)
            gid = ''.join(random.SystemRandom().choices(string.ascii_letters + string.digits, k=12))
            clone_status = CloneStatus(drive, size, query, gid)
            with download_dict_lock:
                download_dict[bmsg.message_id] = clone_status
            sendStatusMessage(query, query.bot)
            result, button = drive.clone(link)
            with download_dict_lock:
                del download_dict[bmsg.message_id]
                count = len(download_dict)
            try:
                if count == 0:
                    Interval[0].cancel()
                    del Interval[0]
                    delete_all_messages()
                else:
                    update_all_messages()
            except IndexError:
                pass
        if bmsg.from_user.username:
            uname = f'@{query.from_user.username}'
        else:
            uname = f'<a href="tg://user?id={query.from_user.id}">{query.from_user.first_name}</a>'
        if uname is not None:
            cc = f'\n\n🙍🏻‍♂️ <b>Penyalin:</b> {uname} - (<code>{query.from_user.id}</code>)\n#id{query.from_user.id}'
            men = f'{uname} '
        if button in ["cancelled", ""]:
            editMessage(men + result, bmsg)
        else:
            editMessage(result + cc, bmsg, button)
        if gdtot_link:
            gd.deletefile(link)


auto_handler = MessageHandler(filters=CustomFilters.authorized_chat & Filters.regex('^(https://drive.google.com|https?://.*\.gdtot\.\S+)') | CustomFilters.authorized_user & Filters.regex(r'^(https://drive.google.com|https?://.*\.gdtot\.\S+)'), callback=auto_button, run_async=True)
clone_callback_handler = CallbackQueryHandler(select_auto, pattern="clone", run_async=True)
count_callback_handler = CallbackQueryHandler(select_auto, pattern="count", run_async=True)
delete_callback_handler = CallbackQueryHandler(select_auto, pattern="delete", run_async=True)
cancel_callback_handler = CallbackQueryHandler(select_auto, pattern="cancelbtn", run_async=True)
dispatcher.add_handler(auto_handler)
dispatcher.add_handler(clone_callback_handler)
dispatcher.add_handler(count_callback_handler)
dispatcher.add_handler(delete_callback_handler)
dispatcher.add_handler(cancel_callback_handler)
