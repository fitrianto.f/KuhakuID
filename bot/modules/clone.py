import random
import string

from telegram.ext import CommandHandler

from bot.helper.mirror_utils.upload_utils import gdriveTools
from bot.helper.telegram_helper.message_utils import sendMessage, sendMarkup, deleteMessage, delete_all_messages, update_all_messages, sendStatusMessage
from bot.helper.telegram_helper.filters import CustomFilters
from bot.helper.telegram_helper.bot_commands import BotCommands
from bot.helper.mirror_utils.status_utils.clone_status import CloneStatus
from bot import dispatcher, SUDO_USERS, OWNER_ID, LOGGER, CLONE_LIMIT, STOP_DUPLICATE, download_dict, download_dict_lock, Interval
from bot.helper.ext_utils.bot_utils import get_readable_file_size, is_gdrive_link, is_gdtot_link, new_thread
from bot.helper.mirror_utils.download_utils.direct_link_generator import gdtot, appdrive_dl
from bot.helper.ext_utils.exceptions import DirectDownloadLinkException

@new_thread
def cloneNode(update, context):
    args = update.message.text.split(" ", maxsplit=1)
    reply_to = update.message.reply_to_message
    if len(args) > 1:
        link = args[1]
        if update.message.from_user.username:
            tag = f"@{update.message.from_user.username}"
        else:
            tag = update.message.from_user.mention_html(update.message.from_user.first_name)
    elif reply_to is not None:
        link = reply_to.text
        if reply_to.from_user.username:
            tag = f"@{reply_to.from_user.username}"
        else:
            tag = reply_to.from_user.mention_html(reply_to.from_user.first_name)
    else:
        link = ''
    gdtot_link = is_gdtot_link(link)
    is_driveapp = True if "driveapp.in" in link else False
    is_appdrive = True if "appdrive.in" in link else False
    if gdtot_link:
        try:
            msg = sendMessage(f"<i>⌛️ Generating GDrive Link from {link}..</i>", context.bot, update)
            link = gdtot(link)
            deleteMessage(context.bot, msg)
        except DirectDownloadLinkException as e:
            deleteMessage(context.bot, msg)
            return sendMessage(str(e), context.bot, update)
    if is_driveapp:
        try:
            msg = sendMessage(f"Memproses link DriveApp:-\n<code>{link}</code>", context.bot, update)
            link = appdrive_dl(link)
            deleteMessage(context.bot, msg)
        except DirectDownloadLinkException as e:
            deleteMessage(context.bot, msg)
            return sendMessage(str(e), context.bot, update)
    if is_appdrive:
        try:
            msg = sendMessage(f"Memproses link AppDrive:- \n<code>{link}</code>", context.bot, update)
            link = appdrive_dl(link)
            deleteMessage(context.bot, msg)
        except DirectDownloadLinkException as e:
            deleteMessage(context.bot, msg)
            return sendMessage(str(e), context.bot, update)
            return sendMessage(str(e), context.bot, update)
    if is_gdrive_link(link):
        gd = gdriveTools.GoogleDriveHelper()
        res, size, name, files = gd.helper(link)
        if res != "":
            return sendMessage(res, context.bot, update)
        if STOP_DUPLICATE:
            LOGGER.info('Checking File/Folder if already in Drive...')
            msg1 = sendMessage(f"<i>Sedang mengecek link kamu...</i>", context.bot, update)
            smsg, button = gd.drive_list(name, True, True)
            if smsg:
                msg3 = f"⚠ <code>{name}</code> (<code>{get_readable_file_size(size)}</code>) <u>sudah ada di GDrive.</u>"
                deleteMessage(context.bot, msg1)
                sendMarkup(msg3, context.bot, update, button)
                if gdtot_link:
                    gd.deletefile(link)
                if is_appdrive:
                    gd.deletefile(link)
                if is_driveapp:
                    gd.deletefile(link)
                return
        if CLONE_LIMIT is not None and update.message.from_user.id not in SUDO_USERS and update.message.from_user.id != OWNER_ID:
            LOGGER.info('Checking File/Folder Size...')
            if size > CLONE_LIMIT * 1024**3:
                msg2 = f'Mohon maaf, limit clone file/folder {CLONE_LIMIT}GB.\n<code>{name}</code> berukuran <code>{get_readable_file_size(size)}</code>.'
                deleteMessage(context.bot, msg1)
                return sendMessage(msg2, context.bot, update)
        if files <= 2:
            deleteMessage(context.bot, msg1)
            msg = sendMessage(f"Sedang menyalin: <code>{link}</code> (<code>{get_readable_file_size(size)}</code>)", context.bot, update)
            result, button = gd.clone(link)
            deleteMessage(context.bot, msg)
        else:
            deleteMessage(context.bot, msg1)
            drive = gdriveTools.GoogleDriveHelper(name)
            gid = ''.join(random.SystemRandom().choices(string.ascii_letters + string.digits, k=12))
            clone_status = CloneStatus(drive, size, update, gid)
            with download_dict_lock:
                download_dict[update.message.message_id] = clone_status
            sendStatusMessage(update, context.bot)
            result, button = drive.clone(link)
            with download_dict_lock:
                del download_dict[update.message.message_id]
                count = len(download_dict)
            try:
                if count == 0:
                    Interval[0].cancel()
                    del Interval[0]
                    delete_all_messages()
                else:
                    update_all_messages()
            except IndexError:
                pass
        cc = f'\n\n🙍🏻‍♂️ <b>Penyalin:</b> {tag}'
        if button in ["cancelled", ""]:
            sendMessage(f"{tag} {result}", context.bot, update)
        else:
            sendMarkup(result + cc, context.bot, update, button)
        if gdtot_link:
            gd.deletefile(link)
        if is_driveapp:
            gd.deletefile(link)
        if is_appdrive:
            gd.deletefile(link)
    else:
        sendMessage('Silahkan ulangi lagi dengan mencantumkan link GDrive:\n<code>/salin (spasi) link GDrive</code>\n\nJika link private, silahkan invite: 4kun@googlegroups.com agar file bisa disalin.', context.bot, update)

clone_handler = CommandHandler(BotCommands.CloneCommand, cloneNode, run_async=True)
dispatcher.add_handler(clone_handler)
