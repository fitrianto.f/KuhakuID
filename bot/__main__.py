import shutil, psutil
import signal
import os
import asyncio
import time
import pytz
import requests
import json
import subprocess
import threading

from pyrogram import idle
from sys import executable
from telegram import ParseMode, InlineKeyboardMarkup
from telegram.ext import CommandHandler, Filters
from telegram.utils import helpers
from datetime import datetime

from wserver import start_server_async
from bot import bot, app, dispatcher, updater, botStartTime, IGNORE_PENDING_REQUESTS, IS_VPS, PORT, alive, web, OWNER_ID, AUTHORIZED_CHATS, LOGGER, Interval, nox, rss_session, INCOMPLETE_TASK_NOTIFIER, DB_URI
from bot.helper.ext_utils import fs_utils
from bot.helper.telegram_helper.bot_commands import BotCommands
from bot.helper.telegram_helper.message_utils import sendMessage, sendMarkup, editMessage, sendLogFile, auto_delete_message
from .helper.ext_utils.telegraph_helper import telegraph
from .helper.ext_utils.bot_utils import get_readable_file_size, get_readable_time
from .helper.ext_utils.db_handler import DbManger
from .helper.telegram_helper.filters import CustomFilters
from bot.helper.telegram_helper import button_build
from .modules import authorize, list, rss, cancel_mirror, search, mirror_status, mirror, clone, watch, shell, eval, delete, speedtest, count, leech_settings

now=datetime.now(pytz.timezone('Asia/Jakarta'))
days = ['Senin', 'Selasa', 'Rabu', 'Kamis', 'Jumat', 'Sabtu', 'Minggu']
month = ['Unknown', 'Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember']
tgl = now.strftime('%d')
tahun = now.strftime('%Y')
jam = now.strftime('%H:%M:%S')

SO_COOL = "bantuan"

def stats(update, context):
    botVersion = subprocess.check_output(["git log -1 --date=format:v%y.%m%d.%H%M --pretty=format:%cd"], shell=True).decode()
    last_commit = subprocess.check_output(["git log -1 --date=short --pretty=format:'%cd <b>From</b> %cr'"], shell=True).decode()
    currentTime = get_readable_time(time.time() - botStartTime)
    current = days[now.weekday()]+', '+tgl+' '+month[now.month]+' '+tahun+' '+jam
    total, used, free = shutil.disk_usage('.')
    total = get_readable_file_size(total)
    used = get_readable_file_size(used)
    free = get_readable_file_size(free)
    sent = get_readable_file_size(psutil.net_io_counters().bytes_sent)
    recv = get_readable_file_size(psutil.net_io_counters().bytes_recv)
    cpuUsage = psutil.cpu_percent(interval=0.5)
    disk = psutil.disk_usage('/').percent
    p_core = psutil.cpu_count(logical=False)
    t_core = psutil.cpu_count(logical=True)
    swap = psutil.swap_memory()
    swap_p = swap.percent
    swap_t = get_readable_file_size(swap.total)
    swap_u = get_readable_file_size(swap.used)
    memory = psutil.virtual_memory()
    mem_p = memory.percent
    mem_t = get_readable_file_size(memory.total)
    mem_a = get_readable_file_size(memory.available)
    mem_u = get_readable_file_size(memory.used)
    stats = f'<b>📈 Statistik @KuHaKuRoBot</b>\n\n' \
            f'<b>Versi Bot:</b> {botVersion}\n'\
            f'<b>Last Commit:</b> {last_commit}\n'\
            f'<b>Bot Uptime:</b> {currentTime}\n'\
            f'<b>Bot Berjalan Sejak: </b> {current}\n\n' \
            f'<b>Total Disk Space:</b> {total}\n'\
            f'<b>Used:</b> {used} | <b>Free:</b> {free}\n\n'\
            f'<b>Upload:</b> {sent}\n'\
            f'<b>Download:</b> {recv}\n\n'\
            f'<b>CPU:</b> {cpuUsage}%\n'\
            f'<b>RAM:</b> {mem_p}%\n'\
            f'<b>DISK:</b> {disk}%\n\n'\
            f'<b>Physical Cores:</b> {p_core}\n'\
            f'<b>Total Cores:</b> {t_core}\n\n'\
            f'<b>SWAP:</b> {swap_t} | <b>Used:</b> {swap_p}%\n'\
            f'<b>Memory Total:</b> {mem_t}\n'\
            f'<b>Memory Free:</b> {mem_a}\n'\
            f'<b>Memory Used:</b> {mem_u}\n'
    msg = sendMessage(stats, context.bot, update)
    threading.Thread(target=auto_delete_message, args=(context.bot, update.message, msg)).start()

def welcome(update, context):
    quote = requests.get("https://animechan.vercel.app/api/random").json() 
    buttons = button_build.ButtonMaker()
    buttons.buildbutton("WEB Anime", "https://kuhaku.id/")
    buttons.buildbutton("BOT Developer", "https://t.me/kuyshare")
    reply_markup = InlineKeyboardMarkup(buttons.build_menu(2))
    welcome_string = f'''
Selamat datang di <b>@KuHaKuRoBot</b>, bot pencari file anime GDrive terbaik.

Klik /bantuan untuk info seputar cara pemakaian dan menu lainnya.

Dikelola dan dikembangkan bersama oleh: <b>KuHaKu.ID</b> dan <b>@KuyShare</b>

=========================
<i>{quote['quote']}</i> ~ <b>{quote['character']}</b> ({quote['anime']})
=========================
'''
    sendMarkup(welcome_string, context.bot, update, reply_markup)

def fitur(update, context):
    fitur_string = f'''
<b>Fitur Lain khusus untuk kamu:</b>
/{BotCommands.PingCommand}: Cek respon bot

/{BotCommands.SpeedCommand}: Cek kecepatan server

/{BotCommands.MirrorCommand}: Download file dan diunggah ke GDrive

/{BotCommands.ZipMirrorCommand}: Download file dan diunggah ke GDrive sebagai .zip

/{BotCommands.UnzipMirrorCommand}: Download file dan diunggah ke GDrive (khusus untuk ekstrak file terkompresi)

/{BotCommands.LeechCommand}: Download file dan diunggah ke Telegram

/{BotCommands.ZipLeechCommand}: Download file dan diunggah ke Telegram sebagai .zip

/{BotCommands.UnzipLeechCommand}: Download file dan diunggah ke Telegram (khusus untuk ekstrak file terkompresi)

/{BotCommands.CloneCommand}: Salin file/folder GDrive

/{BotCommands.CountCommand}: Hitung file/folder GDrive

/{BotCommands.WatchCommand}: Download link Youtube atau link didukung lain dan diunggah ke GDrive

/{BotCommands.LeechWatchCommand}: Download link Youtube atau link didukung lain dan diunggah ke Telegram

/{BotCommands.CancelMirror}: Batalkan proses download

/{BotCommands.ListCommand}: Cari file GDrive

/{BotCommands.StatusCommand}: Cek status download

/{BotCommands.StatsCommand}: Cek statistik bot


<b>CATATAN:</b>
1. Torrent tidak didukung (agar tidak di banned)
2. Fungsi YTDL diperbolehkan
3. Kapasitas bot terbatas, jadi gunakan seperlunya saja.
'''
    sendMessage(fitur_string, context.bot, update)

def kutipan(update, context):
    quote = requests.get("https://animechan.vercel.app/api/random").json()
    kutipan_string = f'''
<i>{quote['quote']}</i>
 ~ <b> {quote['character']} ~ {quote['anime']}</b> ~
'''
    sendMessage(kutipan_string, context.bot, update)

def donasi(update, context):
    donasi_string = f'''
Kuy bantu kami untuk tetap menjalankan bot @KuHaKuRoBot dengan cara berdonasi!

Untuk donasi, silahkan hubungi kami di @Xin_ID
'''
    sendMessage(donasi_string, context.bot, update)

def start(update, context):
    buttons = button_build.ButtonMaker()
    buttons.buildbutton("👉 WEB 👈", "https://kuHaku.id")
    buttons.buildbutton("BOT Dev", "https://t.me/kuyshare")
    reply_markup = InlineKeyboardMarkup(buttons.build_menu(2))
    start_string = f'''
Untuk akses file link private, silahkan hubungi admin atau klik tombol akses dibawah ini.
'''
    sendMarkup(start_string, context.bot, update, reply_markup)

def restart(update, context):
    restart_message = sendMessage("Restarting...", context.bot, update)
    if Interval:
        Interval[0].cancel()
    alive.kill()
    process = psutil.Process(web.pid)
    for proc in process.children(recursive=True):
        proc.kill()
    process.kill()
    fs_utils.clean_all()
    subprocess.run(["python3", "update.py"])
    # Save restart message object in order to reply to it after restarting
    nox.kill()
    with open(".restartmsg", "w") as f:
        f.truncate(0)
        f.write(f"{restart_message.chat.id}\n{restart_message.message_id}\n")
    os.execl(executable, executable, "-m", "bot")


def ping(update, context):
    start_time = int(round(time.time() * 1000))
    reply = sendMessage("Starting Ping", context.bot, update)
    end_time = int(round(time.time() * 1000))
    editMessage(f'{end_time - start_time} ms', reply)


def log(update, context):
    sendLogFile(context.bot, update)


help_string_telegraph = f'''
📍 <b>TIPS SEDERHANA MENCARI FILE VIA @KuYaKuRoBoT</b>
<br/><br/><br/>
Pernah gak ngalamin nyari file tapi gak nemu-nemu? Nah tips ini mungkin bisa membantu kamu.
<br/><br/>
👉 <b>Coba gunakan kata kunci judul lengkap, misal:</b><br/>
/cari the raid<br/>
/cari fantastic four<br/>
/cari avenger endgame 2019<br/><br/>

👉 <b>Gak nemu? Coba ganti spasi dengan titik, misal:</b><br/>
/cari baahubali.the.beginning<br/>
/cari avenger.endgame.2019<br/><br/>

👉 <b>Masih gak nemu? Coba gunakan satu kata kunci, misal:</b><br/>
/cari jumanji<br/>
/cari jurassic<br/><br/>

👉 <b>Masih gak nemu juga? Kemungkinan ada beberapa hal nih:</b><br/>
👉 judul salah ketik (typo)<br/>
👉 judul belum dirilis<br/>
👉 judul sudah rilis tapi tidak ada di database<br/>
👉 judul bener tapi salah tahun<br/>
👉 kata kunci asal-asalan<br/><br/><br/>


<b>FYI!</b><br/>
📌 Data yang dipakai saat ini sangat terbatas, jadi harap maklum. Jika kamu berkenan, silahkan kontribusi dengan mengunakan fitur /salin di bot.
'''

help = telegraph.create_page(
        title='@MesinPencariBot',
        content=help_string_telegraph,
    )["path"]

help_string = f'''
<b>Bantuan Pencarian</b>
1. Untuk mencari file silahkan ketik <code>/cari (spasi) keyword</code>
2. Pilih sumber database pencarian (disarankan pilih <b>SemuaDB</b>)
3. Pilih tipe pencarian
4. Tunggu beberapa saat, jika menemukan hasil silahkan klik tombol yang ada di bawah chat
5. Untuk file minta akses (private), silahkan klik /akses, klik tombol akses, dan klik Gabung Grup

<b>Perintah lain yang tersedia</b>
/cari - Mencari file
/akses - Mendapatkan akses link private
/salin - Kontribusi ke Database
/quotes - Menampilkan kutipan acak
/bantuan - Menampilkan menu ini
/ping - Cek kecepatan respon bot
/stats - Cek statistik bot
'''

def deep_linked_level_1(update, context):
    button = button_build.ButtonMaker()
    button.buildbutton("Tips Pencarian", f"https://telegra.ph/{help}")
    reply_markup = InlineKeyboardMarkup(button.build_menu(1))
    sendMarkup(help_string, context.bot, update, reply_markup)

def bot_help(update, context):
    bot = context.bot
    url = helpers.create_deep_linked_url(bot.username, SO_COOL)
    text = (
        "Untuk akses menu bantuan, silahkan klik tombol berikut!"
    )
    button = button_build.ButtonMaker()
    button.buildbutton("Dapatkan Bantuan", url)
    reply_markup = InlineKeyboardMarkup(button.build_menu(1))
    sendMarkup(text, context.bot, update, reply_markup)
    
def kirim(update,context):
	sendquote = requests.get("https://filesaya.my.id/kutipan/").json()
	sendcuplik = requests.get("https://api.quotable.io/random").json()
	sendtekskutipan = f"<i>{sendquote['quote']}</i> ~ <b>{sendquote['author']}</b>"
	sendtekscuplik = f"<i>{sendcuplik['content']}</i> ~ <b>{sendcuplik['author']}</b>"
	bot.sendMessage(chat_id=-1001517223317, text=sendtekscuplik, parse_mode=ParseMode.HTML)
	bot.sendMessage(chat_id=-1001598769983, text=sendtekscuplik, parse_mode=ParseMode.HTML)
	bot.sendMessage(chat_id=-1001517223317, text=sendtekskutipan, parse_mode=ParseMode.HTML)
	bot.sendMessage(chat_id=-1001598769983, text=sendtekskutipan, parse_mode=ParseMode.HTML)

	
	
botcmds = [
        (f'{BotCommands.ListCommand}','Cari file atau folder GDrive'),
        (f'quotes','Random Anime Quotes'),
        (f'bantuan','Menu Bantuan'),
      ]

def main():
    bot.set_my_commands(botcmds)
    fs_utils.start_cleanup()
    if INCOMPLETE_TASK_NOTIFIER and DB_URI is not None:
        notifier_dict = DbManger().get_incomplete_tasks()
        if notifier_dict:
            for cid, data in notifier_dict.items():
                if os.path.isfile(".restartmsg"):
                    with open(".restartmsg") as f:
                        chat_id, msg_id = map(int, f)
                    msg = 'Restarted successfully! <i>Re-add your task if you want..</i>'
                else:
                    msg = 'Bot Restarted! <i>Re-add your task if you want..</i>'
                for tag, links in data.items():
                     msg += f"\n\n{tag} : "
                     for index, link in enumerate(links, start=1):
                         msg += f" 🔹 <a href='{link}'>Task {index}</a>"
                         if len(msg.encode()) > 4000:
                             if 'Restarted successfully!' in msg and cid == chat_id:
                                 bot.editMessageText(msg, chat_id, msg_id, parse_mode='HTMl')
                                 os.remove(".restartmsg")
                             else:
                                 bot.sendMessage(cid, msg, 'HTML')
                             msg = ''
                if 'Restarted successfully!' in msg and cid == chat_id:
                     bot.editMessageText(msg, chat_id, msg_id, parse_mode='HTMl')
                     os.remove(".restartmsg")
                else:
                    bot.sendMessage(cid, msg, 'HTML')
    if IS_VPS:
        asyncio.run(start_server_async(PORT))
    # Check if the bot is restarting
    if os.path.isfile(".restartmsg"):
        with open(".restartmsg") as f:
            chat_id, msg_id = map(int, f)
        bot.edit_message_text("Restarted successfully!", chat_id, msg_id)
        os.remove(".restartmsg")
    elif OWNER_ID:
        try:
            current = days[now.weekday()]+', '+tgl+' '+month[now.month]+' '+tahun+' '+jam
            botVersion = subprocess.check_output(["git log -1 --date=format:v%y.%m%d.%H%M --pretty=format:%cd"], shell=True).decode()
            last_commit = subprocess.check_output(["git log -1 --date=short --pretty=format:'%cd <b>from</b> %cr'"], shell=True).decode()
            teksquote = requests.get("https://filesaya.my.id/kutipan/").json()
            cuplik = requests.get("https://api.quotable.io/random").json()
            text = f"<b>🟢 Server Menyala.</b>\n⏰ {current}\n\n<b>Versi Bot:</b> <code>{botVersion}</code>\n<b>Last Commit:</b> <code>{last_commit}</code>"
            tekskutipan = f"<i>{teksquote['quote']}</i> ~ <b>{teksquote['author']}</b>"
            tekscuplik = f"<i>{cuplik['content']}</i> ~ <b>{cuplik['author']}</b>"
            bot.sendMessage(chat_id=OWNER_ID, text=text, parse_mode=ParseMode.HTML)
            bot.sendMessage(chat_id=-1001517223317, text=tekscuplik, parse_mode=ParseMode.HTML)
            bot.sendMessage(chat_id=-1001598769983, text=tekscuplik, parse_mode=ParseMode.HTML)
            bot.sendMessage(chat_id=-1001517223317, text=tekskutipan, parse_mode=ParseMode.HTML)
            bot.sendMessage(chat_id=-1001598769983, text=tekskutipan, parse_mode=ParseMode.HTML)
             
            if AUTHORIZED_CHATS:
                for i in AUTHORIZED_CHATS:
                    bot.sendMessage(chat_id=i, text=text, parse_mode=ParseMode.HTML)
        except Exception as e:
            LOGGER.warning(e)

    start_handler = CommandHandler(BotCommands.StartCommand, start, run_async=True)
    ping_handler = CommandHandler(BotCommands.PingCommand, ping, run_async=True)
    restart_handler = CommandHandler(BotCommands.RestartCommand, restart,
                                     filters=CustomFilters.owner_filter | CustomFilters.sudo_user, run_async=True)
    help_handler = CommandHandler(BotCommands.HelpCommand, bot_help, run_async=True)
    stats_handler = CommandHandler(BotCommands.StatsCommand, stats, run_async=True)
    log_handler = CommandHandler(BotCommands.LogCommand, log, filters=CustomFilters.owner_filter | CustomFilters.sudo_user, run_async=True)
    welcome_handler = CommandHandler("start", welcome, run_async=True)
    quote_handler = CommandHandler("quotes", kutipan, run_async=True)
    donasi_handler = CommandHandler("donasi", donasi, run_async=True)
    fitur_handler = CommandHandler("fitur", fitur, filters=CustomFilters.authorized_chat | CustomFilters.authorized_user, run_async=True)
    kirim_handler = CommandHandler("kirim", kirim, run_async=True)
    dispatcher.add_handler(kirim_handler)
    dispatcher.add_handler(fitur_handler)
    dispatcher.add_handler(donasi_handler)
    dispatcher.add_handler(CommandHandler("start", deep_linked_level_1, Filters.regex(SO_COOL)))
    dispatcher.add_handler(start_handler)
    dispatcher.add_handler(ping_handler)
    dispatcher.add_handler(restart_handler)
    dispatcher.add_handler(help_handler)
    dispatcher.add_handler(stats_handler)
    dispatcher.add_handler(log_handler)
    dispatcher.add_handler(welcome_handler)
    dispatcher.add_handler(quote_handler)
    updater.start_polling(drop_pending_updates=IGNORE_PENDING_REQUESTS)
    LOGGER.info("Bot Started!")
    signal.signal(signal.SIGINT, fs_utils.exit_clean_up)
    if rss_session is not None:
        rss_session.start()

app.start()
main()
idle()
