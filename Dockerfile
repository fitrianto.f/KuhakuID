FROM anasty17/mltb:latest
# FROM iamliquidx/mirleechxsdk:a8ce33bccdde0806fbd0541d5faf33e63a572582
# FROM anasty17/mltb-oracle:latest

WORKDIR /usr/src/app
RUN chmod 777 /usr/src/app

COPY yasir_req.txt .
RUN pip3 install --no-cache-dir -r yasir_req.txt

COPY . .

CMD ["bash", "start.sh"]
